%if "%{?__isa_bits}"  == "64"
%define platform linux-g++-64
%endif
%ifarch x86_64 aarch64
%define platform linux-g++
%endif
%define _qt4_prefix %{_libdir}/qt4
%define priority 20
%ifarch x86_64
%define priority 25
%endif

Name:           qt
Epoch:          1
Version:        4.8.7
Release:        51
Summary:        A software toolkit for developing applications
License:        (LGPLv2 with exceptions or GPLv3 with exceptions) and ASL 2.0 and BSD and FTL and MIT
URL:            http://qt-project.org/
Source0:        https://download.qt.io/archive/qt/4.8/4.8.7/qt-everywhere-opensource-src-4.8.7.tar.gz
Source1:        qconfig-multilib.h
Source2:        assistant.desktop
Source3:        designer.desktop
Source4:        linguist.desktop
Source5:        qdbusviewer.desktop
Source6:        qtdemo.desktop
Source7:        qtconfig.desktop
Source8:        hi128-app-qt4-logo.png
Source9:        hi48-app-qt4-logo.png
Source10:       macros.qt4

Patch1:         qt-everywhere-opensource-src-4.8.5-uic_multilib.patch
Patch2:         qt-everywhere-opensource-src-4.8.5-webcore_debuginfo.patch
Patch3:         qt-prefer_adwaita_on_gnome.patch
Patch4:         qt-x11-opensource-src-4.5.1-enable_ft_lcdfilter.patch
Patch5:         qt-everywhere-opensource-src-4.6.3-glib_eventloop_nullcheck.patch
Patch6:         qt-everywhere-opensource-src-4.8.3-qdbusconnection_no_debug.patch
Patch7:         qt-everywhere-opensource-src-4.8.1-linguist_qmake-qt4.patch
Patch8:         qt-everywhere-opensource-src-4.8.1-qt3support_debuginfo.patch
Patch9:         qt-everywhere-opensource-src-4.8.5-qt_plugin_path.patch
Patch10:        qt-everywhere-opensource-src-4.8.4-qmake_pkgconfig_requires_private.patch
Patch11:        qt-everywhere-opensource-src-4.8.7-firebird.patch
Patch12:        qt-everywhere-opensource-src-4.8.7-QT_VERSION_CHECK.patch
Patch13:        qt-x11-opensource-src-4.5.0-fix-qatomic-inline-asm.patch
Patch14:        qt-everywhere-opensource-src-4.8.5-mysql_config.patch
Patch15:        qt-everywhere-opensource-src-4.6.2-cups.patch
Patch16:        qt-everywhere-opensource-src-4.8.7-mariadb.patch
Patch17:        qt-everywhere-opensource-src-4.8.7-qmake_LFLAGS.patch
Patch18:        qt-everywhere-opensource-src-4.8.5-QTBUG-14467.patch
Patch19:        qt-everywhere-opensource-src-4.8.0-tp-qtreeview-kpackagekit-crash.patch
Patch20:        qt-everywhere-opensource-src-4.8.6-s390.patch
Patch21:        qt-everywhere-opensource-src-4.8.3-no_Werror.patch
Patch22:        qt-everywhere-opensource-src-4.8.0-QTBUG-22037.patch
Patch23:        qt-everywhere-opensource-src-4.8.5-QTBUG-21900.patch
Patch24:        qt-everywhere-opensource-src-4.8.5-tds_no_strict_aliasing.patch
Patch25:        qt-everywhere-opensource-src-4.8.0-s390-atomic.patch
Patch26:        qt-everywhere-opensource-src-4.8.3-icu_no_debug.patch
Patch27:        qt-everywhere-opensource-src-4.8.2--assistant-crash.patch
Patch28:        qt-everywhere-opensource-src-4.8.5-QTBUG-4862.patch
Patch29:        qt-4.8-poll.patch
Patch30:        qt-everywhere-opensource-src-4.8.6-QTBUG-37380.patch
Patch31:        qt-everywhere-opensource-src-4.8.6-QTBUG-34614.patch
Patch32:        qt-everywhere-opensource-src-4.8.6-QTBUG-38585.patch
Patch33:        qt-everywhere-opensource-src-4.8.7-mips64.patch
Patch34:        qt-everywhere-opensource-src-4.8.7-gcc6.patch
Patch35:        qt-everywhere-opensource-src-4.8.7-alsa-1.1.patch
Patch36:        qt-everywhere-opensource-src-4.8.7-openssl-1.1.patch
Patch37:        qt-everywhere-opensource-src-4.8.7-icu59.patch
Patch38:        qt-everywhere-opensource-src-4.8.5-qgtkstyle_disable_gtk_theme_check.patch
Patch39:        qt-everywhere-opensource-src-4.8.6-QTBUG-22829.patch
Patch40:        qt-aarch64.patch
Patch41:        qt-everywhere-opensource-src-4.8.5-QTBUG-35459.patch
Patch42:        qt-everywhere-opensource-src-4.8.6-systemtrayicon.patch
Patch43:        stack-protector.patch
Patch44:        0001-Redo-the-Q_FOREACH-loop-control-without-GCC-statemen.patch
Patch6000:      CVE-2018-19869.patch
Patch6001:      CVE-2018-19872.patch
Patch6002:      CVE-2018-19871.patch
Patch6003:      CVE-2018-19870.patch
Patch6004:      CVE-2018-19873.patch
Patch45:        CVE-2020-17507.patch
Patch46:        CVE-2015-9541.patch

BuildRequires:  cups-devel desktop-file-utils gcc-c++ libjpeg-devel findutils libmng-devel libtiff-devel pkgconfig pkgconfig(alsa)
BuildRequires:  pkgconfig(dbus-1) pkgconfig(fontconfig) pkgconfig(glib-2.0) pkgconfig(icu-i18n) openssl-devel pkgconfig(libpng)
BuildRequires:  pkgconfig(libpulse) pkgconfig(xtst) pkgconfig(zlib) rsync pkgconfig(gl) pkgconfig(glu) pkgconfig(ice) pkgconfig(sm)
BuildRequires:  pkgconfig(xcursor) pkgconfig(xfixes) pkgconfig(xft) pkgconfig(xi) pkgconfig(xinerama) pkgconfig(xrandr) pkgconfig(xrender)
BuildRequires:  pkgconfig(xt) pkgconfig(xv) pkgconfig(x11) pkgconfig(xproto)
BuildRequires:  firebird-devel mariadb-connector-c-devel pkgconfig(gtk+-2.0) postgresql-devel unixODBC-devel pkgconfig(sqlite3) freetds-devel
Requires:       ca-certificates qt-settings
Requires(post): %{_sbindir}/update-alternatives
Requires(postun): %{_sbindir}/update-alternatives

Provides:       qgtkstyle = 0.1-1 qt4-sqlite = %{version}-%{release} qt4-sqlite%{?_isa} = %{version}-%{release}
Provides:       qt4 = %{version}-%{release} qt4%{?_isa} = %{version}-%{release}
Provides:       qt-sqlite = %{epoch}:%{version}-%{release} qt-sqlite%{?_isa} = %{epoch}:%{version}-%{release}
Obsoletes:      qgtkstyle < 0.1 qt-sqlite < %{epoch}:%{version}-%{release} qt4 < %{version}-%{release}
Provides:       %{name}-assistant = %{epoch}:%{version}-%{release} qt4-assistant = %{version}-%{release} bundled(clucene09)
Obsoletes:      %{name}-assistant < %{epoch}:%{version}-%{release} %{name}-config < %{epoch}:%{version}-%{release}
Provides:       %{name}-config = %{epoch}:%{version}-%{release} qt4-config = %{version}-%{release}
Obsoletes:      %{name}-demos < %{epoch}:%{version}-%{release} %{name}-doc < %{epoch}:%{version}-%{release} qt4-doc < %{version}-%{release}
Provides:       %{name}-doc = %{epoch}:%{version}-%{release} qt4-doc = %{version}-%{release} %{name}-qvfb = %{epoch}:%{version}-%{release}
Provides:       %{name}-designer-plugin-webkit = %{epoch}:%{version}-%{release} %{name}-demos = %{epoch}:%{version}-%{release}
Obsoletes:      %{name}-designer-plugin-webkit < %{epoch}:%{version}-%{release} %{name}-qvfb < %{epoch}:%{version}-%{release}
Provides:       %{name}-ibase = %{epoch}:%{version}-%{release} qt4-ibase = %{version}-%{release} qt4-ibase%{?_isa} = %{version}-%{release}
Obsoletes:      %{name}-ibase < %{epoch}:%{version}-%{release} %{name}-mysql < %{epoch}:%{version}-%{release} qt-x11 < 1:4.8.5-2
Provides:       %{name}-mysql = %{epoch}:%{version}-%{release} qt4-MySQL = %{version}-%{release} qt4-mysql = %{version}-%{release}
Obsoletes:      qt4-MySQL < %{version}-%{release} qt4-mysql < %{version}-%{release} %{name}-odbc < %{epoch}:%{version}-%{release}
Provides:       qt4-mysql%{?_isa} = %{version}-%{release} %{name}-odbc = %{epoch}:%{version}-%{release} qt4-ODBC = %{version}-%{release}
Provides:       qt4-odbc = %{version}-%{release} qt4-odbc%{?_isa} = %{version}-%{release}  %{name}-postgresql = %{epoch}:%{version}-%{release}
Obsoletes:      qt4-odbc < %{version}-%{release} qt4-ODBC < %{version}-%{release} %{name}-postgresql < %{epoch}:%{version}-%{release}
Provides:       qt4-PostgreSQL = %{version}-%{release} qt4-postgresql = %{version}-%{release} qt4-postgresql%{?_isa} = %{version}-%{release}
Obsoletes:      qt4-PostgreSQL < %{version}-%{release} qt4-postgresql < %{version}-%{release} %{name}-tds < %{epoch}:%{version}-%{release}
Provides:       %{name}-tds = %{epoch}:%{version}-%{release} qt4-tds = %{version}-%{release} qt4-tds%{?_isa} = %{version}-%{release}
Provides:       %{name}-x11 = %{epoch}:%{version}-%{release} %{name}-x11%{?_isa} = %{epoch}:%{version}-%{release} qt4-x11 = %{version}-%{release}
Provides:       qt4-x11%{?_isa} = %{version}-%{release} %{name}-qdbusviewer = %{epoch}:%{version}-%{release} %{name}-common = %{epoch}:%{version}-%{release}
Obsoletes:      qt4-x11 < %{version}-%{release} qt-designer-plugin-phonon < 1:4.7.2-6 %{name}-qdbusviewer < %{epoch}:%{version}-%{release}
Obsoletes:      %{name}-x11 < %{epoch}:%{version}-%{release} %{name}-common < %{epoch}:%{version}-%{release}

%description
Qt (pronounced as "cute", not "cu-tee") is a cross-platform framework that is usually used as a graphical toolkit,
although it is also very helpful in creating CLI applications. It runs on the three major desktop OSes, as well as
on mobile OSes, such as Symbian, Nokia Belle, Meego Harmattan, MeeGo or BB10, and on embedded devices. Ports for
Android (Necessitas) and iOS are also in development

%package        devel
Summary:        Development files for the %{name}
Requires:       %{name} = %{epoch}:%{version}-%{release}
Requires:       pkgconfig(gl) pkgconfig(glu) pkgconfig gcc-c++ pkgconfig(x11) pkgconfig(xproto)
Requires:       pkgconfig(ice) pkgconfig(sm) pkgconfig(xcursor) pkgconfig(xext) pkgconfig(xv) pkgconfig(xt)
Requires:       pkgconfig(xfixes) pkgconfig(xft) pkgconfig(xi) pkgconfig(xinerama) pkgconfig(xrandr) pkgconfig(xrender)
Provides:       qt4-designer = %{version}-%{release} qt4-static = %{version}-%{release}
Provides:       %{name}-static = %{?epoch:%{epoch}:}%{version}-%{release} %{name}-examples = %{epoch}:%{version}-%{release}
Obsoletes:      qt4-devel < %{version}-%{release} qt4-designer < %{version}-%{release}
Provides:       qt4-devel = %{version}-%{release} qt4-devel%{?_isa} = %{version}-%{release}
Provides:       %{name}-devel-private = %{epoch}:%{version}-%{release} qt4-devel-private = %{version}-%{release}
Provides:       qt4-private-devel = %{version}-%{release} %{name}-private-devel = %{epoch}:%{version}-%{release}
Obsoletes:      %{name}-devel-private < %{epoch}:%{version}-%{release} %{name}-examples < %{epoch}:%{version}-%{release}

%description devel
This package contains the development files for %{name}.

%prep
%autosetup -n qt-everywhere-opensource-src-%{version} -p1

sed -i -e 's|^\(QMAKE_STRIP.*=\).*$|\1|g' mkspecs/common/linux.conf

if [ "%{_lib}" == "lib64" ] ; then
  sed -i -e "s,/usr/lib /lib,/usr/%{_lib} /%{_lib},g" config.tests/{unix,x11}/*.test
  sed -i -e "s,/lib /usr/lib,/%{_lib} /usr/%{_lib},g" config.tests/{unix,x11}/*.test
fi

for f in translations/*.ts ; do
  touch ${f%.ts}.qm
done

%build
RPM_OPT_FLAGS=`echo $RPM_OPT_FLAGS | sed 's|-fexceptions||g'`
CXXFLAGS="$CXXFLAGS -std=gnu++98 -Wno-deprecated"

export LD_LIBRARY_PATH=$PWD/lib/
export CXXFLAGS="$CXXFLAGS $RPM_OPT_FLAGS"
export CFLAGS="$CFLAGS $RPM_OPT_FLAGS"
export LDFLAGS="$LDFLAGS $RPM_LD_FLAGS"
export PATH=$PWD/bin:$PATH
export QTDIR=$PWD

./configure -v -fast -optimized-qmake -confirm-license -opensource -prefix %{_qt4_prefix} \
  -bindir %{_qt4_prefix}/bin -datadir %{_qt4_prefix} -docdir %{_docdir}/qt4 -demosdir %{_qt4_prefix}/demos \
  -examplesdir %{_qt4_prefix}/examples -headerdir %{_includedir} -libdir %{_libdir} -importdir %{_qt4_prefix}/imports \
  -plugindir %{_qt4_prefix}/plugins -sysconfdir %{_sysconfdir} -translationdir %{_datadir}/qt4/translations \
  -platform %{platform} -no-rpath -gtkstyle -cups -shared -release -fontconfig -largefile -no-separate-debug-info \
  -no-phonon -no-pch -sm -stl -glib -xkb -xshape -xinerama -xinput -xcursor -xfixes -xrandr -xrender -system-libmng \
  -system-libjpeg -system-libtiff -system-zlib -icu  -openssl-linked -xmlpatterns -dbus-linked  -graphicssystem raster \
  -webkit -plugin-sql-ibase -plugin-sql-mysql -plugin-sql-psql -plugin-sql-odbc -plugin-sql-sqlite -plugin-sql-tds -system-sqlite %{!?webkit:-no-webkit }

make clean -C qmake
%make_build -C qmake \
  QMAKE_CFLAGS_RELEASE="${CFLAGS:-$RPM_OPT_FLAGS}" QMAKE_CXXFLAGS_RELEASE="${CXXFLAGS:-$RPM_OPT_FLAGS}" \
  QMAKE_LFLAGS_RELEASE="${LDFLAGS:-$RPM_LD_FLAGS}" QMAKE_STRIP=

%make_build
%make_build -C tools/qvfb
bin/lrelease translations/*.ts

%install
make install INSTALL_ROOT=%{buildroot}
make install INSTALL_ROOT=%{buildroot} -C tools/qvfb

rsync -aR include/Qt{Core,Declarative,Gui,Script}/private src/{corelib,declarative,gui,script}/*/*_p.h %{buildroot}%{_prefix}/

desktop-file-install --dir=%{buildroot}%{_datadir}/applications --vendor="qt4" \
  %{SOURCE2} %{SOURCE3} %{SOURCE4} %{SOURCE5} %{SOURCE6} %{SOURCE7}

glib2_libs=$(pkg-config --libs glib-2.0 gobject-2.0 gthread-2.0)
ssl_libs=$(pkg-config --libs openssl)
for dep in -laudio -ldbus-1 -lfreetype -lfontconfig -ldl -lphonon -lpthread -lICE ${glib2_libs} \
  -ljpeg -lm -lmng -lpng -lpulse -lpulse-mainloop-glib ${ssl_libs} -lsqlite3 -lz -lSM -lX11 -lXcursor \
  -lXext -lXfixes -lXft -lXinerama -lXi -lXrandr -lXrender -lXt -L/usr/X11R6/lib -L/usr/X11R6/%{_lib} -L%{_libdir} ; do
  sed -i -e "s|$dep ||g" %{buildroot}%{_libdir}/lib*.la 
  sed -i -e "s|$dep ||g" %{buildroot}%{_libdir}/*.prl
done

sed -i -e "/^QMAKE_PRL_BUILD_DIR/d" %{buildroot}%{_libdir}/*.prl
sed -i -e "s|-L%{_builddir}/qt-everywhere-opensource-src-%{version}/lib||g" \
  %{buildroot}%{_libdir}/pkgconfig/*.pc \
  %{buildroot}%{_libdir}/*.prl

rm -f %{buildroot}%{_libdir}/lib*.la

mkdir %{buildroot}%{_bindir}
pushd %{buildroot}%{_qt4_prefix}/bin
for i in * ; do
  case "${i}" in
    assistant|designer|linguist|lrelease|lupdate|moc|qmake|qtconfig|qtdemo|uic)
      ln -v  ${i} %{buildroot}%{_bindir}/${i}-qt4
      ln -sv ${i} ${i}-qt4
      ;;
    qmlviewer)
      ln -v  ${i} %{buildroot}%{_bindir}/${i}
      ln -v  ${i} %{buildroot}%{_bindir}/${i}-qt4
      ln -sv ${i} ${i}-qt4
      ;;
    *)
      ln -v  ${i} %{buildroot}%{_bindir}/${i}
      ;;
  esac
done
popd

pushd %{buildroot}%{_libdir}
for lib in libQt*.so ; do
   libbase=`basename $lib .so | sed -e 's/^lib//'`
   echo "INPUT(-l${libbase})" > lib${libbase}_debug.so 
done
for lib in libQt*.a ; do
   libbase=`basename $lib .a | sed -e 's/^lib//' `
   echo "INPUT(-l${libbase})" > lib${libbase}_debug.a
done
popd

%ifarch x86_64 i686
  mv %{buildroot}%{_includedir}/Qt/qconfig.h %{buildroot}%{_includedir}/QtCore/qconfig-%{__isa_bits}.h
  install -p -m644 -D %{SOURCE1} %{buildroot}%{_includedir}/QtCore/qconfig-multilib.h
  ln -sf qconfig-multilib.h %{buildroot}%{_includedir}/QtCore/qconfig.h
  ln -sf ../QtCore/qconfig.h %{buildroot}%{_includedir}/Qt/qconfig.h
%endif

mkdir -p %{buildroot}%{_sysconfdir}/xdg/qtchooser
pushd    %{buildroot}%{_sysconfdir}/xdg/qtchooser
echo "%{_qt4_prefix}/bin" >  4-%{__isa_bits}.conf
echo "%{_libdir}/qt4" >> 4-%{__isa_bits}.conf
touch default.conf 4.conf
popd
install -p -m644 -D tools/assistant/tools/assistant/images/assistant.png %{buildroot}%{_datadir}/icons/hicolor/32x32/apps/assistant.png
install -p -m644 -D %{SOURCE9} %{buildroot}%{_datadir}/icons/hicolor/48x48/apps/qt4-logo.png
install -p -m644 -D %{SOURCE8} %{buildroot}%{_datadir}/icons/hicolor/128x128/apps/qt4-logo.png
install -p -m644 -D tools/assistant/tools/assistant/images/assistant-128.png %{buildroot}%{_datadir}/icons/hicolor/128x128/apps/assistant.png
install -p -m644 -D tools/designer/src/designer/images/designer.png %{buildroot}%{_datadir}/icons/hicolor/128x128/apps/designer.png
install -p -m644 -D tools/qdbus/qdbusviewer/images/qdbusviewer.png %{buildroot}%{_datadir}/icons/hicolor/32x32/apps/qdbusviewer.png
install -p -m644 -D tools/qdbus/qdbusviewer/images/qdbusviewer-128.png %{buildroot}%{_datadir}/icons/hicolor/128x128/apps/qdbusviewer.png
for icon in tools/linguist/linguist/images/icons/linguist-*-32.png ; do
  size=$(echo $(basename ${icon}) | cut -d- -f2)
  install -p -m644 -D ${icon} %{buildroot}%{_datadir}/icons/hicolor/${size}x${size}/apps/linguist.png
done

cat >%{buildroot}%{_libdir}/pkgconfig/Qt.pc<<EOF
prefix=%{_qt4_prefix}
bindir=%{_qt4_prefix}/bin
datadir=%{_qt4_prefix}
demosdir=%{_qt4_prefix}/demos
docdir=%{_docdir}/qt4
examplesdir=%{_qt4_prefix}/examples
headerdir=%{_includedir}
importdir=%{_qt4_prefix}/imports
libdir=%{_libdir}
moc=%{_qt4_prefix}/bin/moc
plugindir=%{_qt4_prefix}/plugins
qmake=%{_qt4_prefix}/bin/qmake
sysconfdir=%{_sysconfdir}
translationdir=%{_datadir}/qt4/translations

Name: Qt
Description: Configuration For Qt
Version: %{version}
EOF

install -p -m644 -D %{SOURCE10} %{buildroot}%{_rpmconfigdir}/macros.d/macros.qt4
sed -i \
  -e "s|@@NAME@@|%{name}|g" \
  -e "s|@@EPOCH@@|%{?epoch}%{!?epoch:0}|g" \
  -e "s|@@VERSION@@|%{version}|g" \
  -e "s|@@EVR@@|%{?epoch:%{epoch}:}%{version}-%{release}|g" \
  %{buildroot}%{_rpmconfigdir}/macros.d/macros.qt4

mkdir -p %{buildroot}%{_docdir}/qt4/{html,qch,src}

mkdir -p %{buildroot}%{_qt4_prefix}/plugins/{crypto,gui_platform,styles}

rm -fv  %{buildroot}%{_libdir}/libphonon.so*
rm -rfv %{buildroot}%{_libdir}/pkgconfig/phonon.pc
rm -fv  %{buildroot}%{_includedir}/phonon/*.h
rm -rfv %{buildroot}%{_includedir}/phonon*
rm -fv %{buildroot}%{_datadir}/dbus-1/interfaces/org.kde.Phonon.AudioOutput.xml
rm -fv %{buildroot}%{_qt4_prefix}/plugins/designer/libphononwidgets.so
rm -fv %{buildroot}%{_qt4_prefix}/plugins/phonon_backend/*_gstreamer.so
rm -fv %{buildroot}%{_datadir}/kde4/services/phononbackends/gstreamer.desktop
rm -fv %{buildroot}%{_qt4_prefix}/mkspecs/modules/qt_webkit_version.pri
rm -fv %{buildroot}%{_includedir}/Qt/{qgraphicswebview.h,qweb*.h,QtWebKit}
rm -rfv %{buildroot}%{_includedir}/QtWebKit/
rm -rfv %{buildroot}%{_qt4_prefix}/imports/QtWebKit/
rm -fv %{buildroot}%{_libdir}/libQtWebKit*
rm -fv %{buildroot}%{_libdir}/pkgconfig/QtWebKit.pc
rm -rfv %{buildroot}%{_qt4_prefix}/tests/

%find_lang qvfb --with-qt --without-mo
%find_lang qt --with-qt --without-mo
%find_lang assistant --with-qt --without-mo
%find_lang qt_help --with-qt --without-mo
%find_lang qtconfig --with-qt --without-mo
%find_lang qtscript --with-qt --without-mo
cat qvfb.lang qt.lang assistant.lang qt_help.lang qtconfig.lang qtscript.lang >qt_all.lang

%find_lang designer --with-qt --without-mo
%find_lang linguist --with-qt --without-mo
cat designer.lang linguist.lang >qt-devel.lang

%pre
if [ $1 -gt 1 ] ; then
%{_sbindir}/update-alternatives  \
  --remove qtchooser-qt4 \
  %{_sysconfdir}/xdg/qtchooser/qt4-%{__isa_bits}.conf >& /dev/null ||:

%{_sbindir}/update-alternatives  \
  --remove qtchooser-default \
  %{_sysconfdir}/xdg/qtchooser/qt4.conf >& /dev/null ||:
fi

%post
/sbin/ldconfig
%{_sbindir}/update-alternatives \
  --install %{_sysconfdir}/xdg/qtchooser/4.conf \
  qtchooser-4 \
  %{_sysconfdir}/xdg/qtchooser/4-%{__isa_bits}.conf \
  %{priority}

%{_sbindir}/update-alternatives \
  --install %{_sysconfdir}/xdg/qtchooser/default.conf \
  qtchooser-default \
  %{_sysconfdir}/xdg/qtchooser/4.conf \
  %{priority}

touch --no-create %{_datadir}/icons/hicolor ||:

%postun
/sbin/ldconfig
if [ $1 -eq 0 ]; then
%{_sbindir}/update-alternatives  \
  --remove qtchooser-4 \
  %{_sysconfdir}/xdg/qtchooser/4-%{__isa_bits}.conf

%{_sbindir}/update-alternatives  \
  --remove qtchooser-default \
  %{_sysconfdir}/xdg/qtchooser/4.conf
fi

if [ $1 -eq 0 ] ; then
touch --no-create %{_datadir}/icons/hicolor ||:
gtk-update-icon-cache -q %{_datadir}/icons/hicolor 2> /dev/null ||:
fi

%posttrans
gtk-update-icon-cache -q %{_datadir}/icons/hicolor 2> /dev/null ||:

%post devel
touch --no-create %{_datadir}/icons/hicolor ||:

%posttrans devel
gtk-update-icon-cache -q %{_datadir}/icons/hicolor 2> /dev/null ||:
update-desktop-database -q &> /dev/null ||:

%postun devel
if [ $1 -eq 0 ] ; then
touch --no-create %{_datadir}/icons/hicolor ||:
gtk-update-icon-cache -q %{_datadir}/icons/hicolor 2> /dev/null ||:
update-desktop-database -q &> /dev/null ||:
fi

%files -f qt_all.lang
%doc README
%license LICENSE.GPL3 LICENSE.LGPL LGPL_EXCEPTION.txt
%dir %{_sysconfdir}/xdg/qtchooser
%ghost %{_sysconfdir}/xdg/qtchooser/{default.conf,4.conf}
%{_sysconfdir}/xdg/qtchooser/4-%{__isa_bits}.conf
%dir %{_qt4_prefix}
%dir %{_qt4_prefix}/bin
%dir %{_datadir}/qt4
%dir %{_datadir}/qt4/translations/
%dir %{_docdir}/qt4
%dir  %{_docdir}/qt4/{html/,qch/,src/}
%{_docdir}/qt4/html/*
%{_docdir}/qt4/qch/*.qch
%{_docdir}/qt4/src/*
%{_qt4_prefix}/{phrasebooks/,demos/}
%{_qt4_prefix}/plugins/*
%{_libdir}/{libQtCore.so.4*,libQtDBus.so.4*,libQtNetwork.so.4*}
%{_libdir}/{libQtScript.so.4*,libQtSql.so.4*,libQtTest.so.4*}
%{_libdir}/{libQtXml.so.4*,libQtXmlPatterns.so.4*}
%dir %{_qt4_prefix}/plugins
%dir %{_qt4_prefix}/plugins/crypto/
%dir %{_qt4_prefix}/plugins/sqldrivers/
%{_bindir}/{assistant*,qt*config*,qt*demo*,qdbusviewer,qvfb,qdbus}
%{_qt4_prefix}/bin/{assistant*,qt*config*,qt*demo*,qvfb,qdbusviewer,qdbus}
%{_datadir}/applications/{*assistant,*qtconfig}.desktop
%{_datadir}/applications/{*qtdemo,*qdbusviewer}.desktop
%{_datadir}/icons/hicolor/*/apps/assistant*
%exclude %{_docdir}/qt4/qch/{designer.qch,linguist.qch}
%dir %{_qt4_prefix}/imports/
%{_qt4_prefix}/imports/Qt/
%{_libdir}/{libQt3Support,libQtCLucene,libQtDesigner}.so.4*
%{_libdir}/{libQtDeclarative,libQtDesignerComponents}.so.4*
%{_libdir}/{libQtGui,libQtHelp,libQtMultimedia,libQtOpenGL}.so.4*
%{_libdir}/{libQtScriptTools,libQtSvg}.so.4*
%{_datadir}/icons/hicolor/*/apps/{qt4-logo.*,qdbusviewer.*}

%files devel -f qt-devel.lang
%{_rpmconfigdir}/macros.d/macros.qt4
%{_qt4_prefix}/bin/{lconvert,lrelease*,lupdate*,moc*,pixeltool*,linguist*}
%{_qt4_prefix}/bin/{qdoc3*,qmake*,qmlviewer*,qmlplugindump,qt3to4}
%{_qt4_prefix}/bin/{qttracereplay,rcc*,uic*,qcollectiongenerator}
%{_qt4_prefix}/bin/{qdbuscpp2xml,qdbusxml2cpp,qhelpconverter,designer*}
%{_qt4_prefix}/bin/{qhelpgenerator,xmlpatterns,xmlpatternsvalidator}
%{_bindir}/{lrelease*,lupdate*,moc*,uic*,designer*,linguist*,lconvert}
%{_bindir}/{pixeltool,qcollectiongenerator,qdoc3,qmake*,qmlviewer*,qt3to4}
%{_bindir}/{qttracereplay,qdbuscpp2xml,qdbusxml2cpp,qhelpconverter}
%{_bindir}/{qhelpgenerator,qmlplugindump,rcc,xmlpatterns,xmlpatternsvalidator}
%{_includedir}/*
%{_qt4_prefix}/mkspecs/
%{_qt4_prefix}/q3porting.xml
%{_libdir}/{libQt*.so,libQtUiTools*.a,libQt*.prl}
%{_libdir}/pkgconfig/*.pc
%{_datadir}/applications/{*designer.desktop,*linguist.desktop}
%{_datadir}/icons/hicolor/*/apps/{designer*,linguist*}
%{_docdir}/qt4/qch/{designer.qch,linguist.qch}
%{_prefix}/src/{corelib/,declarative/,gui/,script/}
%{_qt4_prefix}/examples/

%changelog
* Mon Sep 21 2020 wutao <wutao61@huawei.com> - 1:4.8.7-51
- fix CVE-2015-9541

* Sun Sep 20 2020 shaoqiang kang <kangshaoqiang1@huawei.com> - 1:4.8.7-50
- fix CVE-2020-17507

* Tue Sep 2020 shaoqiang kang <kangshaoqiang1@huawei.com> - 1:4.8.7-49
- Modify source

* Mon May 25 2020 lizhenhua <lizhenhua12@huawei.com> - 1:4.8.7-48
- Fix compile errors for gcc 9

* Thu Mar 19 2020 yanglijin <yanglijin@huawei.com> - 1:4.8.7-47
- add stack protector

* Wed Jan 15 2020 openEuler Buildteam <buildteam@openeuler.org> - 1:4.8.7-46
- add option %{!?webkit:-no-webkit }

* Wed Dec 25 2019 zhouyihang<zhouyihang1@huawei.com> - 1:4.8.7-45
- Type:cves
- ID:CVE-2018-19870 CVE-2018-19873
- SUG:restart
- DESC: fix CVE-2018-19870 CVE-2018-19873

* Thu Dec 12 2019 shenyangyang<shenyangyang4@huawei.com> - 1:4.8.7-44
- Type:enhancement
- ID:NA
- SUG:NA
- DESC:strenthen spec file

* Wed Oct 30 2019 lirui<lirui130@huawei.com> - 1:4.8.7-43
- Type:cves
- ID:CVE-2018-19871
- SUG:restart
- DESC:fix CVE-2018-19871

* Wed Aug 14 2019 lingsheng<lingsheng@huawei.com> - 1:4.8.7-42
- Type:cves
- ID:CVE-2018-19872
- SUG:restart
- DESC:fix CVE-2018-19872

* Tue May 07 2019 yuejiayan<yuejiayan@huawei.com> - 1:4.8.7-41
- Type:cves
- ID: CVE-2018-19869
- SUG:NA
- DESC:fix CVE-2018-19869

* Thu Apr 10 2019 openEuler Buildteam <buildteam@openeuler.org> - 1:4.8.7-40
- Package init
